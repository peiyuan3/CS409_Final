import React, {useState} from "react";
import "./css/Contact.css";
import "./css/About.css";
import { ToastContainer, toast } from "react-toastify";
import axios from "axios";

const Contact = () => {
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [contactno, setContactno] = useState("");
  const [message, setMessage] = useState("");

  const handleSubmit = (e) => {
    e.preventDefault();
    //console.log(name, email, contactno, message);
    const data = {
      Name: name,
      Email: email,
      ContactNo: contactno,
      Message: message
    }
    axios.post('https://sheet.best/api/sheets/32b30937-d594-4ab2-ab85-dab7e2005286',data).then((response) => {
      toast.success(
        "Thanks for sending us a message. We will contact you soon.!"
      );
      //clearing form
      setName('');
      setEmail('');
      setContactno('');
      setMessage('');
    })
  };

  return (
    <>
      <div id="stars"></div>
      <div id="stars2"></div>
      <div id="stars3"></div>
      <div className="contact-container">
        <div className="screen">
          <div className="screen-header">
            <div className="screen-header-left">
              <div className="screen-header-button close"></div>
              <div className="screen-header-button maximize"></div>
              <div className="screen-header-button minimize"></div>
            </div>
            <div className="screen-header-right">
              <div className="screen-header-ellipsis"></div>
              <div className="screen-header-ellipsis"></div>
              <div className="screen-header-ellipsis"></div>
            </div>
          </div>
          <div className="screen-body">
            <div className="screen-body-item left">
              <div className="app-title">
                <span>CONTACT</span>
                <span>US</span>
              </div>
              <div className="app-contact">CS409-Final project: Expense tracker</div>
            </div>
            <div className="screen-body-item">
            <form
                className="app-form"
                autoComplete="off"
                onSubmit={handleSubmit}
              >

                <div className="app-form-group">
                <input
                    className="app-form-control"
                    placeholder="NAME"
                    type="text"
                    required
                    value={name}
                    onChange={(e) => setName(e.target.value)}
                  />


                </div>
                <div className="app-form-group">
                <input
                    className="app-form-control"
                    placeholder="EMAIL"
                    type="text"
                    required
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                  />

                </div>
                <div className="app-form-group">
                <input
                    className="app-form-control"
                    placeholder="CONTACT NO"
                    type="text"
                    required
                    value={contactno}
                    onChange={(e) => setContactno(e.target.value)}
                  />

                </div>
                <div className="app-form-group mess">
                <input
                    className="app-form-control"
                    placeholder="MESSAGE"
                    type="text"
                    required
                    value={message}
                    onChange={(e) => setMessage(e.target.value)}
                  />

                </div>
                <div className="app-form-group btns">
                  <button className="app-form-button" onClick = {() => {
                    setName('');
                    setEmail('');
                    setContactno('');
                    setMessage('');
                  }}>CANCEL</button>
                  <button type="submit" className="app-form-button">
                    SEND
                  </button>

                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
      <ToastContainer />
    </>
  );
};

export default Contact;
