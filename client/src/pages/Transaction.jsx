import React, { useState, useEffect } from "react";
import axios from "axios";
import "./css/Transaction.css";
import DoughnutChart from "../components/chart/DoughnutChart";
import Form from "../components/Form/Form"
import RecentTransactionList from "../components/recentTransactions/recentTransactions"
import { useNavigate } from "react-router-dom";

const proxy = "https://four09-final.onrender.com/api";

const Transaction = () => {
  const [recentTransactions, setRecentTransactions] = useState(null);
  const [requestError, setrequestError] = useState();
  const [usersData, setUsersData] = useState([]);
  const [transactionStats, setTransationStats] = useState(null);
  const accessToken = sessionStorage.getItem('token');
  const authAxios = axios.create({
    baseURL: proxy,
    headers: {
      Authorization: `Bearer ${accessToken}`, 
    },
  });
  const navigate = useNavigate();
  const yearMonth = new Date().toJSON().slice(0,7);
  const month = new Date().toJSON().slice(5,7);

  const fetchDataTransaction = async () => {
    const monthDaysMap = {"01": "-31", "02": "-28", "03": "-31", "04": "-30", "05": "-31", "06": "-30", "07": "-31", "08": "-31", 
                          "09": "-30", "10": "-31", "11": "-30", "12": "-31"};
    const startDate = yearMonth + "-01";
    const endDate = yearMonth + monthDaysMap[month];
    try {
      const transactions = await authAxios.get(`/transactions_for_user?limit=4&sort={"dateCreated": -1}`);
      const transactionStats = await authAxios.get(`/transaction_stats_for_user?startDate=${startDate}&endDate=${endDate}`);
      const getUsersData = await authAxios.get(`/current_user_info`);
      setRecentTransactions(transactions.data.data);
      setTransationStats(transactionStats.data.data);
      setUsersData(getUsersData.data.data);
    } catch (err) {
      setrequestError(err.response);
    }
  };

  useEffect(() => {
    console.log(sessionStorage.getItem('token'));
    if (!accessToken) {
      // navigate to the login page if user is not logged in
      navigate("/Login")
      return;
    }
    fetchDataTransaction();
    // eslint-disable-next-line
  }, []);

  return (
    <div className="Transction-container">
      <div className="Topbox">
        {usersData.length < 1 ? (
          requestError
        ) : (
          <>
            <h2>
              <span className="userName">Hi! {usersData.userName},</span> your
              spending for the current month ({yearMonth}) spending are as follow:
            </h2>
            
            
          </>
        )}
      </div>
      <div className="Bottombox">
        <div className="leftPanelBox">

        {transactionStats ? 
          <DoughnutChart transactionStats={transactionStats} /> : <></>
        }
        </div>
        <div className="rightPanelBox">
          <Form reloadFunction={fetchDataTransaction}/>
          {recentTransactions ? 
          <RecentTransactionList recentTransactions={recentTransactions}/> : <></>
          }
        </div>
      </div>
    </div>
  );
};

export default Transaction;
