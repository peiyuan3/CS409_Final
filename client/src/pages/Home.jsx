import React from 'react'
import './css/main.css'
import FirstSection from "../components/FirstSection/FirstSection"
import SecondSection from '../components/SecondSection/SecondSection'
const home = () => {
  return (
    <>
      <FirstSection />
      <SecondSection />
    </>
  )
}

export default home