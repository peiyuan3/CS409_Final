import React from "react";
import "./css/About.css";
import Cardprofile from "../components/cardProfile/Cardprofile";


const teamMembers = [ 
  {
    name: "Chien Nguyen",
    title: "Front-end Back-end Engineer",
    quote: "Passionate maniac CS Student and web developer (UIUC)",
    profilePic: "https://i.natgeofe.com/n/548467d8-c5f1-4551-9f58-6817a8d2c45e/NationalGeographic_2572187_square.jpg"
  },
  {
    name: "Barney",
    title: "Front-end Back-end Engineer",
    quote: "Senior CS & Math Student at UIUC",
    profilePic: "https://cdn.britannica.com/39/7139-050-A88818BB/Himalayan-chocolate-point.jpg"
  },
  {
    name: "Xinshuo",
    title: "Front-end Back-end Engineer",
    quote: "Senior CS student at UIUC",
    profilePic: "https://icatcare.org/app/uploads/2018/07/Thinking-of-getting-a-cat.png"
  },
  {
    name: "Tommy",
    title: "Front-end Back-end Engineer",
    quote: "Computer science student interested in web development and IoT devices",
    profilePic: "http://www.alleycat.org/wp-content/uploads/2019/03/FELV-cat.jpg"
  }
]



const about = () => {

  return (
    <>
      <div id="stars"></div>
      <div id="stars2"></div>
      <div id="stars3"></div>
      <div className="about-container">
        
          {teamMembers.map((team,i) => <Cardprofile key={i} team={team}/>)}
        
      </div>
    </>
  );
};

export default about;
