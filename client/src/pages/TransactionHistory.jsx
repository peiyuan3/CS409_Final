import React, { useState, useEffect } from "react";
import axios from "axios";
import "./css/TransactionHistory.css";
import Modal from '@mui/material/Modal';
import { useNavigate, Link } from "react-router-dom";


const proxy = "https://four09-final.onrender.com/api";

const TransactionHistory = () => {

    
    const accessToken = sessionStorage.getItem('token');
    const authAxios = axios.create({
        baseURL: proxy,
        headers: {
        Authorization: `Bearer ${accessToken}`, 
        },
    });
    const navigate = useNavigate();
    const [transactions, setTransactions] = useState(null);
    


    const fetchDataTransaction = async () => {
        try {
          const transactions = await authAxios.get(`/transaction_by_month_for_user`);
          setTransactions(transactions.data.data);
        } catch (err) {
          console.log(err)
        }
    };

    

    useEffect(() => {
        if (!accessToken) {
          // navigate to the login page if user is not logged in
          navigate("/Login")
          return;
        }
        fetchDataTransaction();
        // eslint-disable-next-line 
    }, []);

    return (
        <div className="historyContainer">
            <Link to="/Transaction">
            ← Back to transaction overview
            </Link>
            {transactions? Object.keys(transactions).map(x => 
                <>
                <div className="historyMonth">
                  <h3>{x}</h3> 
                </div>
                {transactions[x].map(oneTransaction => 
                    <TransactionItem data={oneTransaction} reloadFunction={fetchDataTransaction}/>
                )}
                
                </>)
                :
                <></>}
            
        </div>
    )
}

const TransactionItem = ({data, reloadFunction}) => {

    const accessToken = sessionStorage.getItem('token');
    const authAxios = axios.create({
        baseURL: proxy,
        headers: {
        Authorization: `Bearer ${accessToken}`, 
        },
    });


    const [modalOpen, setModalOpen] = useState(false)
    const CATEGORY_COLOR = {
        "Investment": "#2f4f4f",
        "Saving": "#7f0000",
        "Personal": "#008000",
        "Utility": "#4b0082",
        "Grocery": "#d2b48c",
        "Travel": "#ff8c00",
        "Food": "#ffff00",
        "Cash Out": "#00ff00",
        "Entertainment": "#00bfff",
        "Shopping": "#0000ff",
        "Gas": "#ff00ff",
        "Health": "#dda0dd",
        "Professional Service": "#ff1493",
        "Rent": "#7fffd4"
    };

    const handleDelete = async () => {
      try {
        await authAxios.delete(`/transaction/${data._id}`);
        await reloadFunction()
      } catch (err) {
        console.log(err);
      }
      setModalOpen(false);
    }

    return (
      <div>
        <div className="history-itemList">
          <div className="history-itemBox" style={{background: `${CATEGORY_COLOR[data.category]}`}}>{data.category}</div>
          <div className="history-itemInfo">
            <div className="history-item1">{data.description}</div>
            <div className="history-item2">{data.dateCreated.slice(0,10)}</div>
          </div>
          <span className="trashIcon" onClick={() => setModalOpen(true)}>
            <i className="fas fa-trash"></i>
          </span>
        </div>
        <Modal open={modalOpen}>
          <div className="history-modal">
            <div className="modal-prompt">
              Delete {data.description} ({data.dateCreated.slice(0,10)}) ?
            </div>
            <div className="modal-button-container">
              <button className="modal-button" onClick={() => handleDelete()}>confirm </button>
              <button className="modal-button" onClick={() => setModalOpen(false)}>cancel</button>
            </div>
          </div>
      </Modal>
     </div>
    );
}




export default TransactionHistory;