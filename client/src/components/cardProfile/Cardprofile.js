import React from "react";
import "./Cardprofile.css";
const Cardprofile = ({team}) => {
  return (
    <div className="card">
      <div className="pb">
        <img src={team.profilePic}alt=""></img>
      </div>
      <div className="info">
        <h1>{team.name}</h1>
        <h2>{team.title}</h2>
      </div>
      <div className="detail">
        <p>
          "{team.quote}"
        </p>
      </div>
      <div className="buttons">
        <button className="follow">Follow</button>
        <button className="message">Message</button>
      </div>
    </div>
  );
};

export default Cardprofile;
