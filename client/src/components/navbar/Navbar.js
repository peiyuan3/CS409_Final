import React, { useState, useEffect } from "react";
import "./navbar.css";
import { Link } from "react-router-dom";
import Button from "../button/Button";
const Navbar = ({isLoggedIn, setIsLoggedIn}) => {
  const [click, setClick] = useState(false);
  const handleClick = () => {
    setClick(!click);
  };
  const [button, setButton] = useState(true);

  
  const closeMobileMenu = () => {
    setClick(false);
  };

  const clickLoggOut = () => {
    closeMobileMenu();
    handleLogOut();
  }
  const showButton = () => {
    if (window.innerWidth <= 960) {
      setButton(false);
    } else {
      setButton(true);
    }
  };
  window.addEventListener("resize", showButton);


  const handleLogOut = () => {
    sessionStorage.removeItem("token");
    setIsLoggedIn(false);
  };

  useEffect(() => {
    showButton();
    console.log(sessionStorage.getItem('token'));
  }, []);
  return (
    <>
      <nav className="navbar">
        <div className="navbar-container">
          <Link to="/" className="navbar-logo" onClick={closeMobileMenu}>
            <i className="fa-solid fa-sack-dollar"></i> EPTK
          </Link>
          <div className="menu-icon" onClick={handleClick}>
            <i className={click ? "fas fa-times" : "fas fa-bars"} />
          </div>
          <ul className={click ? "nav-menu active" : "nav-menu"}>
            <li className="nav-item">
              <Link to="/" className="nav-links" onClick={closeMobileMenu}>
                Home
              </Link>
            </li>
            <li className="nav-item">
              <Link
                to="/Contact"
                className="nav-links"
                onClick={closeMobileMenu}
              >
                Contact Us
              </Link>
            </li>
            <li className="nav-item">
              <Link to="/About" className="nav-links" onClick={closeMobileMenu}>
                About Us
              </Link>
            </li>
            <li className="nav-item">
            {isLoggedIn && 
              <Link to="/Transaction" className="nav-links" onClick={closeMobileMenu}>
                Transaction
              </Link>}
            </li>
            <li>
              {
              !isLoggedIn &&
                <Link
                to="/Login"
                className="nav-links-mobile"
                onClick={closeMobileMenu}
                >
                Log In
              </Link>}
              {isLoggedIn && 
              <Link
              to="/"
              className="nav-links-mobile"
              onClick={clickLoggOut}
              >
              Log Out
            </Link>}
              
            </li>
          </ul>
          {button && !isLoggedIn && <Button buttonStyle="btn-outline0" link={"/Login"}>Log In</Button>}
          
          {button && isLoggedIn && <Button buttonStyle="btn-outline0" onClick={handleLogOut} link={"/"}>Log Out</Button>}
          
        </div>
      </nav>
    </>
  );
};

export default Navbar;
