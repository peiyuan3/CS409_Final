// ./components/PieChart.js
import React from "react";
import { Doughnut } from "react-chartjs-2";
import { Chart, ArcElement } from "chart.js";
import "./DoughnutChart.css";
import Labels from "./Labels";
Chart.register(ArcElement);

const DoughnutChart = ({ transactionStats }) => {

  const CATEGORY_COLOR = {
    "Investment": "#2f4f4f",
    "Saving": "#7f0000",
    "Personal": "#008000",
    "Utility": "#4b0082",
    "Grocery": "#d2b48c",
    "Travel": "#ff8c00",
    "Food": "#ffff00",
    "Cash Out": "#00ff00",
    "Entertainment": "#00bfff",
    "Shopping": "#0000ff",
    "Gas": "#ff00ff",
    "Health": "#dda0dd",
    "Professional Service": "#ff1493",
    "Rent": "#7fffd4"
  };

  const data = transactionStats.map(x => x["sum"]);
  const categories = transactionStats.map(x => x["_id"]);
  const correspondingColor = categories.map(x => CATEGORY_COLOR[x]);
  const config = {
    data: {
      datasets: [
        {
          data: data,
          backgroundColor: correspondingColor,
          hoverOffser: 5,
          borderRadius: 10,
          spacing: 5,
        },
      ],hoverOffset: 10
    },
    options: {
      cutout: 125,
    },
  };

  return (
    <div className="chartContainer">
      <div className="item">
        <h1>Expense Analyzing</h1>
        <div className="chart">
          <Doughnut {...config} />
          <h3 className="total">
            <span className="amount"> ${data.reduce((partialSum, a) => partialSum + a, 0)}</span>
          </h3>
        </div>
        <Labels data={transactionStats}/>
      </div>
    </div>
  );
};
export default DoughnutChart;
