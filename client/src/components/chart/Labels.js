import React from "react";
import "./DoughnutChart.css";

function toFixed(num, fixed) {
  fixed = fixed || 0;
  fixed = Math.pow(10, fixed);
  return Math.floor(num * fixed) / fixed;
}

//["#2f4f4f", "#7f0000", "#008000", "#4b0082", "#d2b48c", "#ff8c00"
//, "#ffff00", "#00ff00", "#00bfff", "#0000ff", "#ff00ff", "#dda0dd", "#ff1493", "#7fffd4"]
const Labels = ({data}) => {

  const CATEGORY_COLOR = {
    "Investment": "#2f4f4f",
    "Saving": "#7f0000",
    "Personal": "#008000",
    "Utility": "#4b0082",
    "Grocery": "#d2b48c",
    "Travel": "#ff8c00",
    "Food": "#ffff00",
    "Cash Out": "#00ff00",
    "Entertainment": "#00bfff",
    "Shopping": "#0000ff",
    "Gas": "#ff00ff",
    "Health": "#dda0dd",
    "Professional Service": "#ff1493",
    "Rent": "#7fffd4"
  };
  
  const total = data.map(x => x["sum"]).reduce((partialSum, a) => partialSum + a, 0);
  

  const items = data.map(x => { return { type: x["_id"], 
                                         color: CATEGORY_COLOR[x["_id"]], 
                                         percent: toFixed((x["sum"] / total) * 100, 2)} });

  return (
  <>
  {items.map((item,i)=> 
    <LabelComponents key={i} data={item}/>
  )}
  </>)
}

function LabelComponents({data}) {
  if(!data) return <></>;
  return (
    <div className="labels">
      <div className="labelTitles">
        <div className="colorItems" style={{ background: data.color ?? '#fff'}}></div>
        <h3>{data.type ?? ''}</h3>
      </div>
      <h3>{data.percent ?? 0}%</h3>
    </div>
  );
}
export default Labels;
