import React from "react";
import "./SecondSection.css";
import {Link} from 'react-router-dom'
const SecondSection = () => {
  return (
    <div className="Section-container">
        <h1>What can the expense tracker do?</h1>
      <div className="inner-container">
        <div className="left-panel-section">
          <div className="imageChart">
            <img
              className="chartHome"
              src={process.env.PUBLIC_URL + "/pics/chart.png"}
              alt=""
            ></img>
          </div>
        </div>
        <div className="right-panel-section">
          <h1>Visualize your transactions by using chart</h1>
          <p>
            "With chart, you can easily see how much you spend, and what
            category you spend most"
          </p>
        </div>
      </div>

      <div className="inner-container">
        <div className="left-panel-section-2">
          <h1>Track all your transactions by categories</h1>
          <p>
            "Category is one of the powerful task can help you see what have you spent"
          </p>
        </div>
        <div className="right-panel-section-2">
          <div className="imageChart">
            <img
              className="chartHome"
              src={process.env.PUBLIC_URL + "/pics/categories.png"}
              alt=""
            ></img>
          </div>
        </div>
      </div>
      <h1>For more information  <Link to="/login"><span className="JoinUs">Join us now!</span></Link></h1>
    </div>
  );
};

export default SecondSection;
