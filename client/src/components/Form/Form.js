import {React, useState} from "react";
import axios from 'axios';
import "./Form.css";
import { ToastContainer, toast } from 'react-toastify';




const Form = ( props ) => {
  const [transactionState, setTransactionState] = useState({category: "Investment"});
  const accessToken = sessionStorage.getItem("token");
  const proxy = "https://four09-final.onrender.com/api";
  

  const authAxios = axios.create({
    baseURL: proxy,
    headers: {
      Authorization: `Bearer ${accessToken}`,
    },
  });

  const handleFormChange = async (e) => {
    e.preventDefault();
    try {
      //console.log(transactionState)
      const postUsersData = await authAxios.post(`/transactions_for_user`, transactionState);
      console.log(postUsersData);
      props.reloadFunction()
      toast("Successfully added!");
    } catch (err) {
      console.log(err.response.data);
      if (err.response.status === 500) {
        toast.error("Please enter both Description and Amount!");
      }      
    }
  }

  const handleDateChange = (date) => {
    const today = new Date().toJSON().slice(0,10);
    // if date is today
    // use the current time (with hours) so that transactions added for today are ordered correctly
    if (date === today) {
      setTransactionState({...transactionState, dateCreated: new Date().toISOString()})
    } else {
      setTransactionState({...transactionState, dateCreated: new Date(date).toISOString()})
    }
  }
  return (
    <form className="transactionForm" id="transactionForm" onSubmit={handleFormChange}>
      <h1 className="Title">Transaction</h1>
      <div className="inputField">
        {/* description */}
        <h3>Description: </h3>
        <input
          className="Input-text titleName"
          type="text"
          placeholder="House, Rental,..." onChange={(e) => setTransactionState({...transactionState, description: e.target.value})}
          value={transactionState.description}
        ></input>
        {/* category */}
        <h3>Type: </h3>
        <select className="dropdown-input" onChange={(e) => setTransactionState({...transactionState, category: e.target.value})}>
          <option value="Investment" defaultValue>Investment</option>
          <option value="Saving">Saving</option>
          <option value="Personal">Personal</option>
          <option value="Utility">Utility</option>
          <option value="Grocery">Grocery</option>
          <option value="Travel">Travel</option>
          <option value="Food">Food</option>
          <option value="Cash Out">Cash Out</option>
          <option value="Entertainment">Entertainment</option>
          <option value="Shopping">Shopping</option>
          <option value="Gas">Gas</option>
          <option value="Health">Health</option>
          <option value="Professional Service">Professional Service</option>
          <option value="Rent">Rent</option>
        </select>
        {/* amount */}
        <h3>Amount: </h3>
        <input
          className="Input-text"
          type="number"
          placeholder="Amount... $0" onChange={(e) => setTransactionState({...transactionState, amount: e.target.value})}
          value={transactionState.amount}
        ></input>
        <h3>Date: </h3>
        <input
          className="Input-date"
          type="date"
          onChange={(e) => {handleDateChange(e.target.value)}}
        />
        <button className="button-62" type="submit">Add transaction</button>
      </div>
      <ToastContainer/>
    </form>
  );
};

export default Form;

