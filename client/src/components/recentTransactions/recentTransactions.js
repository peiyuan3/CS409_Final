import React from "react";
import "./recentTransactions.css";
import { Link } from "react-router-dom";


const RecentTransactionList = ({recentTransactions}) => {

  const CATEGORY_COLOR = {
    Investment: "#2f4f4f",
    Saving: "#7f0000",
    Personal: "#008000",
    Utility: "#4b0082",
    Grocery: "#d2b48c",
    Travel: "#ff8c00",
    Food: "#ffff00",
    "Cash Out": "#00ff00",
    Entertainment: "#00bfff",
    Shopping: "#0000ff",
    Gas: "#ff00ff",
    Health: "#dda0dd",
    "Professional Service": "#ff1493",
    Rent: "#7fffd4",
  };

  const items = recentTransactions.map((x) => {
    return {
      name: `${x["description"]}    $${x["amount"]}`,
      color: CATEGORY_COLOR[x["category"]],
      date: x['dateCreated']
    };
  });
  return (
    <div className="transContainer">
      <h1>Recent Transactions</h1>
      {items.map((item, i) => (
        <Transaction key={i} data={item}></Transaction>
      ))}
      <Link to="/TransactionHistory">View all transactions</Link>
    </div>
  );
};

function Transaction({ data }) {
  console.log(data);
  return (
    <div className="itemList">
      <span className="itemBox" style={{background: `${data.color ?? "#fff"}`}}></span>
      <span className="item1">{data.name ?? ""}</span>
      <span className="item1">{data.date.slice(0, 10)}</span>
      {/* <span className="trashIcon"><i className="fas fa-trash"></i></span> */}
    </div>
  );
}

export default RecentTransactionList;
